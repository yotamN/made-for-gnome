function filterApplications() {
	const value = $searchInput.value;
	let count = 0;

	for (let i=0; i<applications.length; i++) {
		const name = applications[i]['Name'];
		const summary = applications[i]['Summary'];
		const description = applications[i]['Description']['Value'];

		if (name.toUpperCase().includes(value.toUpperCase()) ||
		    description.toUpperCase().includes(value.toUpperCase()) ||
		    summary.toUpperCase().includes(value.toUpperCase())) {
			document.getElementById(applications[i]['ID']).style.display = 'flex';
			count += 1;
		} else {
			document.getElementById(applications[i]['ID']).style.display = 'none';
		}
	}

	if (count == 0) {
		document.querySelector('.no-results').style.display = 'block';
	} else {
		document.querySelector('.no-results').style.display = 'none';
	}
}

const $searchInput = document.querySelector('.global-search');
$searchInput.style.display = 'block';

$searchInput.addEventListener('keyup', filterApplications);
filterApplications();
