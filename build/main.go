package main

import (
	"encoding/xml"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

type Component struct {
	ID          string      `xml:"id"`
	Name        string      `xml:"name"`
	Summary     string      `xml:"summary"`
	Links       []Link      `xml:"url"`
	Description Description `xml:"description"`
}

type Description struct {
	Value string `xml:",innerxml"`
}

type Link struct {
	Type string `xml:"type,attr"`
	URL  string `xml:",chardata"`
}

func getAppdataFiles(dir string) ([]string, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return []string{}, err
	}

	var result []string
	for _, f := range files {
		fullPath, err := filepath.Abs(filepath.Join(dir, f.Name()))
		if err != nil {
			return result, err
		}
		result = append(result, fullPath)
	}

	return result, nil
}

func parseAppdataFiles(files []string) ([]Component, error) {
	var parsedAppdatas []Component

	for _, f := range files {
		appdata, err := parseAppdata(f)
		if err != nil {
			return parsedAppdatas, err
		}

		parsedAppdatas = append(parsedAppdatas, appdata)
	}

	return parsedAppdatas, nil
}

func parseAppdata(path string) (Component, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return Component{}, err
	}

	var appdata Component
	xml.Unmarshal(bytes, &appdata)

	return appdata, nil
}

func getHomepage(component Component) (string, error) {
	for _, link := range component.Links {
		if link.Type == "homepage" {
			return link.URL, nil
		}
	}

	return "", fmt.Errorf("link of type homepage does not exist for %v", component.ID)
}

func escapeHTML(components []Component) []Component {
	for _, c := range components {
		c.Description.Value = template.HTMLEscapeString(c.Description.Value)
	}

	return components
}

func renderHTML(w io.Writer, components []Component) error {
	t, err := template.New("index.tmpl").Funcs(map[string]interface{}{
		"getHomepage": getHomepage,
		"escapeHTML":  escapeHTML,
	}).ParseFiles("index.tmpl")
	if err != nil {
		return err
	}

	return t.Execute(w, components)
}

func main() {
	appdataDir := os.Args[1]
	files, err := getAppdataFiles(appdataDir)
	if err != nil {
		log.Fatalln("Failed to get appdata files:", err)
	}

	appdatas, err := parseAppdataFiles(files)
	if err != nil {
		log.Fatalln("Failed to parse appdata files:", err)
	}

	err = renderHTML(os.Stdout, appdatas)
	if err != nil {
		log.Fatalln("Failed to render HTML:", err)
	}
}
